import { CIRLPage } from './app.po';

describe('cirl App', function() {
  let page: CIRLPage;

  beforeEach(() => {
    page = new CIRLPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
