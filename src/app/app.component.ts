import { Component } from '@angular/core';

//import { Sample } from './sample';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	
/*samples : Sample[] =[
    {
      description:`This is a simple calculator using .Net. Lorem ipsum dolor sit amet, 
      consectetur adipiscing elit. Duis tristique placerat rhoncus. Donec at justo diam. 
      Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
       Cras ut lobortis velit. Quisque vestibulum auctor faucibus. Sed varius ac sem eget feugiat. 
       Phasellus pellentesque tortor mi, in accumsan odio condimentum a. Sed imperdiet lorem sapien,
        non elementum mi porttitor nec. In non blandit nisl, a placerat turpis. Maecenas facilisis 
        hendrerit nulla quis convallis. Proin tempor lectus porta velit eleifend consectetur. 
        Vivamus ultrices erat lorem, sed ultricies ipsum dictum nec. Nullam at diam lacinia,
         pulvinar dui mattis, dictum justo. Integer non lectus augue.`,
        overlayImgUrl: '/app/images/rect-purple.png',
        screenshotUrl: '/app/images/rect-purple.png',
        rightFooterTitle: 'The SIMPLE CALCULATOR: Not so simple to implement',
        overlayTitle: 'Simple Calculator'
    },
    {
      description:`This is the code for the current site Lorem ipsum dolor sit amet, consectetur 
        adipiscing elit. Duis tristique placerat rhoncus. Donec at justo diam. Cum sociis natoque 
        penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras ut lobortis velit. 
        Quisque vestibulum auctor faucibus. Sed varius ac sem eget feugiat. Phasellus pellentesque 
        tortor mi, in accumsan odio condimentum a. Sed imperdiet lorem sapien, non elementum mi porttitor
         nec. In non blandit nisl, a placerat turpis. Maecenas facilisis hendrerit nulla quis convallis. 
         Proin tempor lectus porta velit eleifend consectetur. Vivamus ultrices erat lorem, sed ultricies 
         ipsum dictum nec. Nullam at diam lacinia, pulvinar dui mattis, dictum justo. Integer non lectus 
         augue.`,
        overlayImgUrl: '/app/images/orange-rect.png',
        screenshotUrl: '/app/images/orange-rect.png',
        rightFooterTitle: 'Code In Real Life: A simple A2 app with room to grow.',
        overlayTitle: 'C.I.R.L'
    }
  ];
*/  
}
