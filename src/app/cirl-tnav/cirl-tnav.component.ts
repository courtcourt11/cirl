import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cirl-tnav',
  templateUrl: './cirl-tnav.component.html',
  styleUrls: ['./cirl-tnav.component.css']
})
export class CirlTnavComponent implements OnInit {

  isAriaExpanded : boolean;
  title = 'Code In Real Life';

  constructor() {
  	this.isAriaExpanded = false;
   }

   toggleAriaExpanded(){
   	this.isAriaExpanded = !this.isAriaExpanded;
   }
  ngOnInit() {
  }

}
