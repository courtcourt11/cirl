import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CirlSampleComponent } from './cirl-sample/cirl-sample.component';
import { CirlTnavComponent } from './cirl-tnav/cirl-tnav.component';

import { routing } from './app.routing';
import { CirlExperienceComponent } from './cirl-experience/cirl-experience.component';
import { CirlToolkitComponent } from './cirl-toolkit/cirl-toolkit.component';
import { CirlSamplesComponent } from './cirl-sample/cirl-samples.component';
import { HomeComponent } from './home/home.component';
import { CirlContactComponent } from './cirl-contact/cirl-contact.component';

@NgModule({
  declarations: [
    AppComponent,
    CirlSampleComponent,
    CirlTnavComponent,
    CirlExperienceComponent,
    CirlToolkitComponent,
    CirlSamplesComponent,
    HomeComponent,
    CirlContactComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
