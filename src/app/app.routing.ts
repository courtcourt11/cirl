import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import { CirlSamplesComponent } from './cirl-sample/cirl-samples.component';
import { CirlToolkitComponent } from './cirl-toolkit/cirl-toolkit.component';
import { CirlExperienceComponent } from './cirl-experience/cirl-experience.component';
import { HomeComponent } from './home/home.component';

const appRoutes : Routes = [
	{ path: 'toolkit', component : CirlToolkitComponent },
	{ path: 'experience', component : CirlExperienceComponent },
	{ path: 'code', component : CirlSamplesComponent },
	{ path: '', component : HomeComponent }
];

export const routing : ModuleWithProviders = RouterModule.forRoot(appRoutes);


