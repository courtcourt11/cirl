import { Component , Input, trigger, state, animate, transition, style, keyframes, group} from '@angular/core';

import { Sample } from './sample';


@Component({
  selector: 'app-cirl-sample',
  templateUrl: './cirl-sample.component.html',
  styleUrls: ['./cirl-sample.component.css'],
  animations: [
    trigger('dismissBox', [
      state('shown', style({opacity:1})),
      state('hidden',style({opacity:0, transform:'translateX(480px)'})),
      /*transition('shown => hidden',animate('500ms', keyframes([
        style({transform: 'translateX(0)', offset: 0}),
          style({opacity:1, transform: 'translateX(480px)', offset: 0.8}),
          style({opacity:.5, offset: 0.9}),
          style({opacity: 0,   offset: 1.0})
        ])))*/
      /*transition('shown => hidden',[style({transform: 'translateX(0)'}), 
        group([animate('1500ms',style({opacity:1, transform: 'translateX(480px)', offset:0})),
          //animate('500ms',style({opacity: 1,   offset: .7})),
          animate('2000ms 1000ms',style({opacity: 0,   offset: 1}))  
      ])])*/
      transition('shown => hidden',animate('500ms'))
    ]),
    trigger('showRightTitle',[
      state('hidden',style({opacity:0})),
      state('shown', style({opacity:1})),
      transition('hidden => shown',animate('700ms 600ms'))
    ]),
    trigger('showScreenshot',[
      state('hidden',style({opacity:0})),
      state('shown', style({opacity:1})),
      transition('hidden => shown',animate('700ms 1000ms'))
    ])
  ]
  
})


export class CirlSampleComponent {
  

  currentOverlayState : string;
  currentRightTitleState : string;
  currentScreenshotState: string;

  constructor(){
    this.currentOverlayState = 'shown';
    this.currentRightTitleState = 'hidden';
    this.currentScreenshotState = 'hidden';

  }

  slideFade(){
    this.currentOverlayState = 'hidden';
    this.currentRightTitleState = 'shown';
    this.currentScreenshotState = 'shown';
  }

  //Getting/binding values from the parent component
  @Input() sampleItem: Sample;

}
