export class Sample{
	description : string;
	overlayImgUrl : string;
	screenshotUrl : string;
	rightFooterTitle: string;
	overlayTitle : string;
	repoUrl : string;
	demoUrl : string;
}