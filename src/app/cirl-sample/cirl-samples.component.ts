import { Component, OnInit } from '@angular/core';

import { Sample } from './sample';

@Component({
  selector: 'app-cirl-samples',
  templateUrl: './cirl-samples.component.html',
  styleUrls: ['./cirl-samples.component.css']
})
export class CirlSamplesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  samples : Sample[] =[
    {
      description:`Called "Simple Calculator" because I thought "How hard could it be?" when
      I started it.(famous last words) Written in C# on the DotNet framework. It's not asynchronous, 
      so updates will include the asynchronous control implementation that wasn't possible initially.  
      Page states between postbacks maintained in the ViewState object which made the logic complicated.
      Simple styling that I will improve on later.

      Needs testing for value limits. I'm not sure that the datatypes that I chose for the variables
      are a best fit given the values that they could possibly have. Also needs more thorough functional testing
      of input/operation button click combinations. Of course, robust exception handling for all of the above. But it works
      well enough for basic calculations.`,
        overlayImgUrl: '/app/images/rect-purple.png',
        screenshotUrl: '/app/images/rect-purple.png',
        rightFooterTitle: 'The Simple Calculator: Not so simple to implement',
        overlayTitle: 'Simple Calculator',
        repoUrl:'http://www.google.com',
        demoUrl:'http://www.google.com'
    },
    {
      description:`Code for the site you are currently viewing. My first full-fledged Angular2
        app with routing, data binding, animation, layout design, events,  parent-child component interaction, and some 
        slightly hacked Bootswatch since Bootstrap 4 is not complete, and Bootswatch is only compatible with 
        Bootstrap 3 at the time of creating it. Got to do a dab of graphics design  (my nod to Ubuntu) for the layout.
        Updates to include unit tests and a better way of handling the templated data
        sample data. Normally, I'd use a relational database on the backend but that's not an option here. More
        to come as I dive deeper into Angular2.`,
        overlayImgUrl: '/app/images/orange-rect.png',
        screenshotUrl: '/app/images/orange-rect.png',
        rightFooterTitle: 'Code In Real Life: A small Angular2 app.',
        overlayTitle: 'C.I.R.L',
        repoUrl:'http://www.google.com',
        demoUrl:'http://www.google.com'
    }
  ];


}
